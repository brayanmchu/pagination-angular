import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  posts = [];

  constructor(
    private dataService: DataService, 
    private route: ActivatedRoute,
    ){
   this.dataService.getData().subscribe(data =>{
   this.posts = data;
   });
  }

  ngOnInit(){
    
  }

  handlePage(e: PageEvent){
    this.page_size= e.pageSize
    this.page_number = e.pageIndex + 1
  }


  page_size: number = 10
  page_number: number =  1
  pageSizeOptions = [10]



 
}
